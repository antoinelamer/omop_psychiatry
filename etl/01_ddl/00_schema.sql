----------------------------------------
--
-- OMOP Psychiatry
--
-- Antoine Lamer
--
-- 08/12/19
--
-- Creation of schemas
--
-- raw_data = schema with raw data
-- etl = schema for etl transformations
-- omop = final OMOP schema
--
----------------------------------------

-- SCHEMA: raw_data
----------------------------------------

DROP SCHEMA if exists raw_data ;

CREATE SCHEMA if not exists raw_data;
	
	
-- SCHEMA: etl
----------------------------------------

DROP SCHEMA if exists etl ;

CREATE SCHEMA if not exists etl;	
	
	
-- SCHEMA: omop
----------------------------------------

DROP SCHEMA if exists omop ;

CREATE SCHEMA if not exists omop;
	
-- SCHEMA: achilles
----------------------------------------

DROP SCHEMA if exists achilles ;

CREATE SCHEMA if not exists achilles;

-- Create as many cohort shema as you need
-- SCHEMA: cohort_
----------------------------------------

DROP SCHEMA if exists cohort ;

CREATE SCHEMA if not exists cohort;

-- SCHEMA: result

DROP SCHEMA if exists result ;

CREATE SCHEMA if not exists result;