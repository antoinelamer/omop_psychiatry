----------------------------------------
--
--
-- Antoine Lamer
--
-- 08/12/19
--
-- DDL Sequence for OMOP Tables
--
----------------------------------------

-- location_id_seq
----------------------------------------------

DROP SEQUENCE if exists etl.location_id_seq;

CREATE SEQUENCE if not exists etl.location_id_seq INCREMENT 1 START 1 MINVALUE 1;

ALTER SEQUENCE etl.location_id_seq OWNER TO postgres;


-- care_site_id_seq
----------------------------------------------

DROP SEQUENCE if exists etl.care_site_id_seq;

CREATE SEQUENCE if not exists etl.care_site_id_seq INCREMENT 1 START 1 MINVALUE 1;

ALTER SEQUENCE etl.care_site_id_seq OWNER TO postgres;

-- person_id_seq
----------------------------------------------

DROP SEQUENCE if exists  etl.person_id_seq;

CREATE SEQUENCE if not exists etl.person_id_seq INCREMENT 1 START 1 MINVALUE 1;

ALTER SEQUENCE etl.person_id_seq OWNER TO postgres;
	

-- observation_period_id_seq
----------------------------------------------

DROP SEQUENCE if exists etl.observation_period_id_seq;

CREATE SEQUENCE if not exists etl.observation_period_id_seq INCREMENT 1 START 1 MINVALUE 1;

ALTER SEQUENCE etl.observation_period_id_seq OWNER TO postgres;

-- visit_occurrence_id_seq
----------------------------------------------

DROP SEQUENCE if exists etl.visit_occurrence_id_seq;

CREATE SEQUENCE if not exists etl.visit_occurrence_id_seq INCREMENT 1 START 1 MINVALUE 1;

ALTER SEQUENCE etl.visit_occurrence_id_seq OWNER TO postgres;	
	
	
-- visit_detail_id_seq
----------------------------------------------

DROP SEQUENCE if exists etl.visit_detail_id_seq;

CREATE SEQUENCE if not exists etl.visit_detail_id_seq INCREMENT 1 START 1 MINVALUE 1;

ALTER SEQUENCE etl.visit_detail_id_seq OWNER TO postgres;	
	
-- condition_occurrence_id_seq
----------------------------------------------

DROP SEQUENCE if exists etl.condition_occurrence_id_seq;

CREATE SEQUENCE if not exists etl.condition_occurrence_id_seq INCREMENT 1 START 1 MINVALUE 1;

ALTER SEQUENCE etl.condition_occurrence_id_seq OWNER TO postgres;

-- drug_exposure_id_seq
----------------------------------------------

DROP SEQUENCE if exists etl.drug_exposure_id_seq;

CREATE SEQUENCE if not exists etl.drug_exposure_id_seq INCREMENT 1 START 1 MINVALUE 1;

ALTER SEQUENCE etl.drug_exposure_id_seq OWNER TO postgres;

-- procedure_occurrence_id_seq
----------------------------------------------

DROP SEQUENCE if exists etl.procedure_occurrence_id_seq;

CREATE SEQUENCE if not exists etl.procedure_occurrence_id_seq INCREMENT 1 START 1 MINVALUE 1;

ALTER SEQUENCE etl.procedure_occurrence_id_seq OWNER TO postgres;
	
-- measurement_id_seq
----------------------------------------------

DROP SEQUENCE if exists etl.measurement_id_seq;

CREATE SEQUENCE if not exists etl.measurement_id_seq INCREMENT 1 START 1 MINVALUE 1;

ALTER SEQUENCE etl.measurement_id_seq OWNER TO postgres;

-- note_id_seq
----------------------------------------------

DROP SEQUENCE if exists etl.note_id_seq;

CREATE SEQUENCE if not exists etl.note_id_seq INCREMENT 1 START 1 MINVALUE 1;

ALTER SEQUENCE etl.note_id_seq OWNER TO postgres;

-- observation_id_seq
----------------------------------------------

DROP SEQUENCE if exists etl.observation_id_seq;

CREATE SEQUENCE if not exists etl.observation_id_seq INCREMENT 1 START 1 MINVALUE 1;

ALTER SEQUENCE etl.observation_id_seq OWNER TO postgres;
	
	
-- episode_id_seq
----------------------------------------------

DROP SEQUENCE if exists etl.episode_id_seq;

CREATE SEQUENCE if not exists etl.episode_id_seq INCREMENT 1 START 1 MINVALUE 1;

ALTER SEQUENCE etl.episode_id_seq OWNER TO postgres;
	
	
--
-- concept_lille_id_seq
----------------------------------------------

DROP SEQUENCE if exists etl.concept_lille_id_seq;

CREATE SEQUENCE if not exists etl.concept_lille_id_seq INCREMENT 1 START 1 MINVALUE 1;

ALTER SEQUENCE etl.concept_lille_id_seq OWNER TO postgres;

