----------------------------------------
--
-- ETL OMOP PSYCHIATRY
--
-- Table : CDM_SOURCE
--
-- Antoine Lamer
--
-- 23/03/2020
--
--
----------------------------------------


INSERT INTO omop.cdm_source(
	cdm_source_name, 
	cdm_source_abbreviation, 
	cdm_holder, 
	source_description, 
	source_documentation_reference, 
	cdm_etl_reference, 
	source_release_date, 
	cdm_release_date, 
	cdm_version, 
	vocabulary_version
)
VALUES (
	'Lille University Hospital - Psychiatry', 
	'CHU Lille - Psychiatry', 
	'Psychiatry', 
	'Hospital data of patient who had a visit in psychiatry department', 
	NULL::text, 
	'', 
	'2019-12-31', 
	'2019-02-06', 
	'6.0.0', 
	'2019-12-19');
	
	
