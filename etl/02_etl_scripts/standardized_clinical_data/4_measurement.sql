----------------------------------------
--
-- ETL OMOP PSYCHIATRY
--
-- Antoine Lamer
--
-- 2020-03-28
--
-- 1) Extract raw data
-- 2) Transform and map to standard concept_id
-- 3) Load into omop_anesthesia.visit_occurrence
--
----------------------------------------

/*
Schemas:
raw_data = schema with raw data
etl = schema for etl transformations
omop = final OMOP schema
*/

------------------------------------------------------------
-- Source 1 : Measurement 
-- heart rate, arterial pressure, ...
------------------------------------------------------------


-- 1) Extract measurement value, parameter, unit, date and 
-- operation identifier from the source table
------------------------------------------------------------

drop table if exists etl.measurement_extract;

create table if not exists etl.measurement_extract as 
--
select idpatient::text as person_source_value
		, date as measurement_datetime
		, echelle as measurement_source_value
		, score as value_as_number
from raw_data.echelles;

select * from etl.measurement_extract;

-- 2) Transform
--
-- Mapping with concept_id
-- 
-- person_id
-- visit_occurrence_id
-- visit_detail_id
--
-- Value, keep only numeric value ?
-- 
----------------------------------------------------

-- visit_detail_id
-- measurement_source_concept_id
-- unit_source_concept_id

drop table if exists etl.measurement_tr_1;

create table if not exists etl.measurement_tr_1 as 
select 	0 as measurement_id
		, coalesce(p.person_id, 0) as person_id
		, 0::integer as measurement_concept_id
		, measurement_datetime::date as measurement_date
		, measurement_datetime
		, null::varchar as measurement_time
		, 0::integer as measurement_type_concept_id -- to do
		, null::integer as operator_concept_id
		, value_as_number 
		, null::integer as value_as_concept_id
		, 0::integer as unit_concept_id
		, null::numeric as range_low
		, null::numeric as range_high
		, 0 as provider_id
		, 0::integer as visit_occurrence_id
		, 0::integer as visit_detail_id
		, measurement_source_value
		, coalesce(cm.concept_id, 0) as measurement_source_concept_id
		, null::text as unit_source_value
		, 0::integer as unit_source_concept_id
		, value_as_number::text as value_source_value
from etl.measurement_extract me
-- person_id
left outer join omop.person p
on me.person_source_value = p.person_source_value
-- measurement_source_concept_id
left outer join omop.concept cm
on me.measurement_source_value = cm.concept_code
and cm.vocabulary_id = 'Lille P Scales'
;

select * from etl.measurement_tr_1;

--
-- Get measurement_concept_id
-------------------------------------------------------------------------------

drop table if exists etl.measurement_tr;

create table if not exists etl.measurement_tr as 
select 	0 as measurement_id
		, person_id
		, coalesce(cr1.concept_id_2, 0) as measurement_concept_id
		, measurement_date
		, measurement_datetime
		, measurement_time
		, measurement_type_concept_id  -- TODO
		, operator_concept_id
		, value_as_number
		, value_as_concept_id
		, unit_concept_id
		, range_low
		, range_high
		, provider_id
		, visit_occurrence_id
		, visit_detail_id
		, measurement_source_value
		, measurement_source_concept_id
		, unit_source_value
		, value_source_value
from etl.measurement_tr_1 tr1
-- measurement_concept_id
left outer join omop.concept_relationship cr1
on tr1.measurement_source_concept_id = cr1.concept_id_1
;

select * from etl.measurement_tr where measurement_concept_id = 0;


-- 3) Load into omop.measurement
----------------------------------------------------

delete from omop.measurement;
	
alter sequence etl.measurement_id_seq restart with 1;

insert into omop.measurement (
		measurement_id
		, person_id
		, measurement_concept_id
		, measurement_date
		, measurement_datetime
		, measurement_time
		, measurement_type_concept_id
		, operator_concept_id
		, value_as_number
		, value_as_concept_id
		, unit_concept_id
		, range_low
		, range_high
		, provider_id
		, visit_occurrence_id
		, visit_detail_id
		, measurement_source_value
		, measurement_source_concept_id
		, unit_source_value
		, value_source_value
)
-----
select nextval('etl.measurement_id_seq')
		, person_id
		, measurement_concept_id
		, measurement_date
		, measurement_datetime
		, measurement_time
		, measurement_type_concept_id
		, operator_concept_id
		, value_as_number
		, value_as_concept_id
		, unit_concept_id
		, range_low
		, range_high
		, provider_id
		, visit_occurrence_id
		, visit_detail_id
		, measurement_source_value
		, measurement_source_concept_id
		, unit_source_value
		, value_source_value
from etl.measurement_tr
;

