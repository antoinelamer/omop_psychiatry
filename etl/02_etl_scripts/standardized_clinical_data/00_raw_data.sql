
---------------------------------------------------------------------------------------

drop table if exists raw_data.patient;

create table if not exists raw_data.patient (
	id integer,
	identifiant character varying(20),
	birthyear character varying(20)
	);
	
select * from raw_data.patient;

COPY raw_data.patient FROM '/media/ant/data_crypt/ds/psychiatry/200421/Patients.csv'
WITH DELIMITER E';' CSV HEADER QUOTE E'"' ENCODING 'UTF-8'
;

---------------------------------------------------------------------------------------

drop table if exists raw_data.echelles;

create table if not exists raw_data.echelles (
	id integer,
	idPatient integer,
	categorie character varying(20),
	echelle character varying(20),
	resultat character varying,
	date timestamp,
	score integer
	);
	
select * from raw_data.echelles;

COPY raw_data.echelles FROM '/media/ant/data_crypt/ds/psychiatry/200421/Echelles.csv'
WITH DELIMITER E';' CSV HEADER QUOTE E'"' ENCODING 'UTF-8'
;