----------------------------------------
--
-- ETL OMOP PSYCHIATRY
--
-- Table : VISIT_DETAIL
--
-- Antoine Lamer
--
-- 14/12/19
--
-- 2 sources :
-- operating room (AIMS)
-- unit stay (PMSI)
----------------------------------------

/*
Schemas:
raw_data = schema with raw data
etl = schema for etl transformations
omop = final OMOP schema
*/
