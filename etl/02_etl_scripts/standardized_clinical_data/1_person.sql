----------------------------------------
--
-- ETL OMOP PSYCHIATRY
--
-- Antoine Lamer
--
-- 03/12/19
--
----------------------------------------

/*
Schemas:
raw_data = schema with raw data
etl = schema for etl transformations
omop = final OMOP schema
*/

-- Person
----------------------------------------------------

-- Extract patient_id, birth_date, sex
----------------------------------------------------

drop table etl.person_extract;

delete from omop.person;

-- Raw data
insert into omop.person (
	person_id
	, gender_concept_id
	, year_of_birth
	, race_concept_id
	, ethnicity_concept_id
	, person_source_value
	, gender_source_concept_id
	, race_source_concept_id
	, ethnicity_source_concept_id)
select nextval('etl.person_id_seq')
		, 0::integer as gender_concept_id
	   	, coalesce(birthyear::integer, 1900) as year_of_birth
		, 0::integer as race_concept_id
		, 0::integer as ethnicity_concept_id
	   	, id::text as person_source_value
		, 0::integer as gender_source_concept_id
		, 0::integer as race_source_concept_id
		, 0::integer as ethnicity_source_concept_id
from raw_data.patient -- 1 row per operation
;
