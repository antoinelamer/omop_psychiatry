----------------------------------------
--
-- ETL 
--
-- Table : VISIT_OCCURRENCE
--
-- Antoine Lamer
--
-- 2020-03-27
--
-- 1) Extract patient_id, birth_date, sex from the source table
-- 2) Transform and mapp to OHDSI concept_id
-- 3) Load into omop_anesthesia.visit_occurrence
--
----------------------------------------

/*
Schemas:
raw_data = schema with raw data
etl = schema for etl transformations
omop = final OMOP schema
*/


