----------------------------------------
--
-- ETL OMOP PSYCHIATRY
--
-- Table : CARE_SITE, FACT_RELATIONSHIP
--
-- Antoine Lamer
--
-- 25/12/19
--
-- 1) Extract hospitals, medical units, operating rooms
-- 2) Transform and mapp to OHDSI concept_id, map to fact_relationship
-- 3) Load into omop.care_site
--
----------------------------------------

/*
Schemas:
raw_data = schema with raw data
etl = schema for etl transformations
omop = final OMOP schema
*/
