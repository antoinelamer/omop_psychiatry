----------------------------------------
--
-- ETL OMOP PSYCHIATRY
--
-- Table : LOCATION
--
-- Antoine Lamer
--
-- 25/12/19
--
--
----------------------------------------


INSERT INTO omop.location(
	location_id
	, address_1
	, address_2
	, city
	, state
	, zip
	, county
	, country
	, location_source_value
	, latitude
	, longitude)
VALUES (
	nextval('etl.care_site_id_seq')
	, '2 avenue Oscar Lambret'
	, null
	, 'Lille'
	, null
	, 59000
	, 'Nord'
	, 'France'
	, 'CHU Lille'
	, null
	, null);
	
